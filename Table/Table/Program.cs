﻿using System;
using System.Linq;

static class Program
{
    const int maxWidth = 10;
    const int maxHeight = 10;
    const int maxNumber = 999;
    static int maxLengthOfWidth = maxNumber.ToString().Length;


    static void Main(string[] args)
    {
        int width = ReadNumber("Введите ширину таблицы: ", x => x > 0 && x <= maxWidth, $"Введите число в диапазон от 1 до {maxWidth}");
        int height = ReadNumber("Введите высоту таблицы: ", x => x > 0 && x <= maxHeight, $"Введите число в диапазон от 1 до {maxHeight}");

        Console.Clear();
        Random rnd = new Random();

        DrawSeparator(width, '╔', '╦', '╗');
        for (int i = 0; i < height; i++)
        {
            if (i == 1)
            {
                DrawSeparator(width, '╠', '╬', '╣');
            }
            for (int j = 0; j < width; j++)
            {
                Console.Write('║');
                if (i == 0)
                {
                    Console.Write(CenterText($"{j}", maxLengthOfWidth));
                }
                else
                {
                    Console.Write(CenterText(rnd.Next(maxNumber + 1).ToString(), maxLengthOfWidth));
                }
            }
            Console.WriteLine('║');
        }
        DrawSeparator(width, '╚', '╩', '╝');

        Console.ReadKey();
    }

    static string CenterText(string text, int neededLength)
    {
        int missingSpace = neededLength - text.Length;
        return string.Join("", new string(' ', (missingSpace + 1) / 2), text, new string(' ', missingSpace / 2));
    }

    static void DrawSeparator(int length, char left, char middle, char right)
    {
        Console.Write(left);
        Console.Write(string.Join(middle, Enumerable.Repeat(new string('═', maxLengthOfWidth), length)));
        Console.WriteLine(right);
    }

    static int ReadNumber(string text, Predicate<int> condition, string errorMessage)
    {
        int result;
        while (true)
        {
            Console.Write(text);
            if (int.TryParse(Console.ReadLine(), out result) && condition(result))
                break;
            Console.Clear();
            Console.WriteLine(errorMessage);
        }
        return result;
    }
}